function Calculate() {
    let f1 = document.getElementsByName("цена");
    let f2 = document.getElementsByName("количество");
    let res = document.getElementById("результат");
    if ((/^[0-9]+$/).test(f1[0].value) && (/^[0-9]+$/).test(f2[0].value)) {
        res.innerHTML = "Итоговая цена: " + (f1[0].value * f2[0].value);
    } else {
        res.innerHTML = "Возникла ошибка";
        return false;
    }
}
window.addEventListener("DOMContentLoaded", function () {
    let button = document.getElementById("button_1");
    button.addEventListener("click", Calculate);
});


